@extends('layout.template')

@section('title', 'My Profile')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Hello, My name is {{$name}} :D</h1>
        </div>
    </div>
</div>  
@endsection