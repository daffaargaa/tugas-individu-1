<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/profile', function () {
    $name = 'Ruhrei';
    return view('profile',['name' => $name ]);
});

Route::get('/storage', function () {
    $bag = ['Pencil', 'Pen', 'Book', 'Eraser'];
    return view('storage', ['bag' => $bag]);
});
